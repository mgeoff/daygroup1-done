#pragma once

class Sounds
{
public:
	void MenuMusic();
	void BlasterSound();
	void CollisionSound();
	void StopSound();
};