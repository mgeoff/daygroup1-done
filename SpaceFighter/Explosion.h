#pragma once

#include "GameObject.h"
#include "Weapon.h"

class Explosion : public GameObject
{

public:

	virtual void LoadContent(ResourceManager* pResourceManager);

	virtual void Draw(SpriteBatch* pSpriteBatch);

	virtual void Update(const GameTime* pGameTime);

	virtual void Activate(Vector2 &position);

	virtual std::string ToString() const { return "Explosion"; } // pure-virtual in GameObject (MUST override)

	virtual CollisionType GetCollisionType() const { return CollisionType::NONE; } // pure-virtual in GameObject

private:

	Texture* m_pTexture;

	static Level* s_pCurrentLevel;

	int m_counter = 5;
};
