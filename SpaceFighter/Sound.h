#pragma once
#include <allegro5/allegro_audio.h>
#include <string>


class ContentManager;

enum Playmode
{
    BiDir,
    Loop,
    Once,
    StreamOnce,
    StreamOneDir
};

class Sound : public Trackable
{
private:
    /*  Variables
    * * * * * * * * * * * * */
    ALLEGRO_SAMPLE* pSample;

    float
        mGain,
        mPan,
        mSpeed;

    Playmode mPlaymode;

    std::string
        mAssetPath,
        mAssetName;

    /*  Private Functions
    * * * * * * * * * * * * */
    ALLEGRO_PLAYMODE getPlaymode(Playmode playmode)
    {
        switch (playmode)
        {
        case BiDir:
            return ALLEGRO_PLAYMODE::ALLEGRO_PLAYMODE_BIDIR;

        case Loop:
            return ALLEGRO_PLAYMODE::ALLEGRO_PLAYMODE_LOOP;

        case Once:
            return ALLEGRO_PLAYMODE::ALLEGRO_PLAYMODE_ONCE;

        case StreamOnce:
            return ALLEGRO_PLAYMODE::_ALLEGRO_PLAYMODE_STREAM_ONCE;

        case StreamOneDir:
            return ALLEGRO_PLAYMODE::_ALLEGRO_PLAYMODE_STREAM_ONEDIR;

            // Default to once
        default:
            return ALLEGRO_PLAYMODE::ALLEGRO_PLAYMODE_ONCE;
        }
    }

public:
    /*  Constructors/Destructor
    * * * * * * * * * * * * */
    Sound();

    Sound(
        // assetPath, assetName, gain, pan, speed, playmode
        std::string assetPath,
        std::string assetName,
        float gain = 1.0f,
        float pan = 0.0f,
        float speed = 1.0f,
        Playmode playmode = Once);

    Sound(const Sound& other);

    ~Sound();

    friend class ContentManager; // My content system.

    void play();

};