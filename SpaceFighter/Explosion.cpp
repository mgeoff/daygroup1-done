#include "Explosion.h"
#include "GameObject.h"
#include "Level.h"


void Explosion::LoadContent(ResourceManager* pResourceManager)
{
	m_pTexture = pResourceManager->Load<Texture>("Textures\\Explosion.png");
}

void Explosion::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter());
	}
}

void Explosion::Update(const GameTime* pGameTime)
{
	if (m_counter > 0)
	{
		m_counter--;
	}
	else
	{
		Deactivate();
	}
}

void Explosion::Activate(Vector2 &position)
{
	SetPosition(position);
	GameObject::Activate(); // if we call just "Activate(postion)"... we get a recursive function that never ends
							// ...because Activate(position) is the funciton we are in...
							// instead we need to call activate in the parent class
}